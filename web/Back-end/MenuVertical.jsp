<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>
    <body>
        <aside class="main-sidebar">
            <section class="sidebar">
                <div class="user-panel">
                    <div class="pull-left image">
                        <img src="dist/img/admin.png" class="img-circle" alt="User Image">
                    </div>
                    <div class="pull-left info">
                        <p>Rosa Gutierrez</p>
                        <a href="#"><i class="fa fa-circle text-success"></i>Conectado</a>
                    </div>
                </div>
                <ul class="sidebar-menu" data-widget="tree">
                    <li class="header">MENU DE NAVEGACION</li>
                    <li class="treeview active">
                        <a href="index.html">
                            <i class="fa fa-dashboard"></i><span>Inicio</span>
                        </a>
                    </li>
                    <li class="treeview">
                        <a href="#">
                            <i class="fa fa-users"></i>
                            <span>Recurso Humano</span>
                            <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
                        </a>
                        <ul class="treeview-menu">
                            <li><a href="usuarios.jsp"><i class="fa fa-circle-o"></i>Usuarios</a></li>
                        </ul>
                    </li>
                    <li class="treeview">
                        <a href="#">
                            <i class="fa fa-users"></i>
                            <span>Administrar</span>
                            <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
                        </a>
                        <ul class="treeview-menu">
                            <li><a href="Categorias.jsp"><i class="fa fa-circle-o"></i>Categorias</a></li>
                            <li><a href="Libros.jsp"><i class="fa fa-circle-o"></i>Libros</a></li>
                        </ul>
                    </li>
                </ul>
            </section>
        </aside>
    </body>
</html>
