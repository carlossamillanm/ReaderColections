package Servlets;

import Beans.Usuario;
import Negocio.nUsuario;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet(name = "sUsuario", urlPatterns = {"/sUsuario"})
public class sUsuario extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            nUsuario nUser = new nUsuario();
            HttpSession sesion = request.getSession();
            ArrayList<Usuario> user;
            Usuario usu = new Usuario();

            if (request.getParameter("opcion").equals("Registrar")) {

                String nombre = request.getParameter("nombre");
                String Apellidop = request.getParameter("apaterno");
                String Apellidom = request.getParameter("amaterno");
                String Dni = request.getParameter("dni");
                String Sexo = "";
                String Correo = request.getParameter("correo");
                String Celular = request.getParameter("celular");
                String clave = request.getParameter("clave");
                String cuenta = request.getParameter("cuenta");
                String Tipo = "A";

                if (request.getParameter("sexo").equals("Femenino")) {
                    Sexo = "F";
                }
                if (request.getParameter("sexo").equals("Masculino")) {
                    Sexo = "M";
                }

                usu = new Usuario(null, nombre, Apellidop, Apellidom, Dni, Sexo, Correo, Celular, cuenta, clave, Tipo);

                nUser.RegistrarUsuario(usu);
                response.sendRedirect("Back-end/usuarios.jsp");
            }

            if (request.getParameter("opcion").equals("ListarUsuarios")) {
                List<Usuario> lista = new ArrayList<Usuario>();
                lista = nUser.ListarUsuarios();
                request.setAttribute("ListarUsuarios", lista);
            }

            if (request.getParameter("opcion").equals("Eliminar")) {
                String id = request.getParameter("us_id");
                Integer Id = Integer.parseInt(id);
                nUser.Eliminar(Id);
                response.sendRedirect("Back-end/usuarios.jsp");
            }

            if (request.getParameter("opcion").equals("Modificar")) {

                String id = request.getParameter("u_id");
                String nombre = request.getParameter("u_nombre");
                String Apellidop = request.getParameter("u_apaterno");
                String Apellidom = request.getParameter("u_amaterno");
                String Dni = request.getParameter("u_dni");
                String Sexo = "";
                String Correo = request.getParameter("u_correo");
                String Celular = request.getParameter("u_celular");
                String clave = request.getParameter("u_clave");
                String cuenta = request.getParameter("u_cuenta");
                String Tipo = "A";

                if (request.getParameter("u_sexo").equals("Femenino")) {
                    Sexo = "F";
                }
                if (request.getParameter("u_sexo").equals("Masculino")) {
                    Sexo = "M";
                }
                
                usu = new Usuario(Integer.parseInt(id), nombre, Apellidop, Apellidom, Dni, Sexo, Correo, Celular, cuenta, clave, Tipo);

                nUser.ModificarUsuario(usu);
                response.sendRedirect("Back-end/usuarios.jsp");
            }
        } catch (Exception e) {

        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
