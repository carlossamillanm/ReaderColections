package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class index_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\r\n");
      out.write("<!DOCTYPE html>\r\n");
      out.write("<html>\r\n");
      out.write("    <head>\r\n");
      out.write("        <title>Readers Colections</title>\r\n");
      out.write("        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\r\n");
      out.write("        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />\r\n");
      out.write("        <meta name=\"keywords\" content=\"Elite Shoppy Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, \r\n");
      out.write("              Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design\" />\r\n");
      out.write("        <script type=\"application/x-javascript\"> addEventListener(\"load\", function() { setTimeout(hideURLbar, 0); }, false);\r\n");
      out.write("            function hideURLbar(){ window.scrollTo(0,1); } </script>\r\n");
      out.write("        <link href=\"css/bootstrap.css\" rel=\"stylesheet\" type=\"text/css\" media=\"all\" />\r\n");
      out.write("        <link href=\"css/estilos.css\" rel=\"stylesheet\" type=\"text/css\" media=\"all\" />\r\n");
      out.write("        <link href=\"css/font-awesome.css\" rel=\"stylesheet\"> \r\n");
      out.write("        <link href=\"css/easy-responsive-tabs.css\" rel='stylesheet' type='text/css'/>\r\n");
      out.write("    </head>\r\n");
      out.write("    <body background=\"//Imagenes/fondo.png\">\r\n");
      out.write("        ");
      org.apache.jasper.runtime.JspRuntimeLibrary.include(request, response, "Header.jsp", out, false);
      out.write("\r\n");
      out.write("        ");
      org.apache.jasper.runtime.JspRuntimeLibrary.include(request, response, "Menu.jsp", out, false);
      out.write("\r\n");
      out.write("            <!-- banner -->\r\n");
      out.write("            <br>\r\n");
      out.write("            <br>\r\n");
      out.write("            <div class=\"container\">\r\n");
      out.write("                <div id=\"carousel-1\" class=\"carousel slide\" data-ride=\"carousel\">\r\n");
      out.write("                    <div class=\"carousel-inner\" role=\"listbox\">\r\n");
      out.write("                        <div class=\"item active\"  align=\"center\">\r\n");
      out.write("                            <img src=\"Imagenes/Slider/portada1.jpg\" class=\"img-responsive\" alt=\"\"  >\r\n");
      out.write("                            <div class=\"container\">\r\n");
      out.write("                                <div class=\"carousel-caption\"><p class=\"contenido-slider\">Portada 1</p></div>\r\n");
      out.write("                            </div>\r\n");
      out.write("                        </div>\r\n");
      out.write("                        <div class=\"item\"  align=\"center\">\r\n");
      out.write("                            <img src=\"Imagenes/Slider/portada2.jpg\" class=\"img-responsive\" alt=\"\"  >\r\n");
      out.write("                            <div class=\"container\">\r\n");
      out.write("                                <div class=\"carousel-caption\"><p class=\"contenido-slider\">Portada 1</p></div>\r\n");
      out.write("                            </div>\r\n");
      out.write("                        </div>\r\n");
      out.write("                        <div class=\"item\"  align=\"center\">\r\n");
      out.write("                            <img src=\"Imagenes/Slider/portada3.jpg\" class=\"img-responsive\" alt=\"\"  >\r\n");
      out.write("                            <div class=\"container\">\r\n");
      out.write("                                <div class=\"carousel-caption\"><p class=\"contenido-slider\">Portada 1</p></div>\r\n");
      out.write("                            </div>\r\n");
      out.write("                        </div>\r\n");
      out.write("                        <div class=\"item\"  align=\"center\">\r\n");
      out.write("                            <img src=\"Imagenes/Slider/portada4.jpg\" class=\"img-responsive\" alt=\"\"  >\r\n");
      out.write("                            <div class=\"container\">\r\n");
      out.write("                                <div class=\"carousel-caption\"><p class=\"contenido-slider\">Portada 1</p></div>\r\n");
      out.write("                            </div>\r\n");
      out.write("                        </div>\r\n");
      out.write("                    </div>\r\n");
      out.write("                    <!-- Controls -->\r\n");
      out.write("                    <a href=\"#carousel-1\" class=\"left carousel-control\" role=\"button\" data-slide=\"prev\">\r\n");
      out.write("                        <span class=\"icon-prev\"></span>\r\n");
      out.write("                    </a>\r\n");
      out.write("                    <a href=\"#carousel-1\" class=\"right carousel-control\" data-slide=\"next\">\r\n");
      out.write("                        <span class=\"icon-next\"></span>\r\n");
      out.write("                    </a>\r\n");
      out.write("                    <!-- Indicators -->\r\n");
      out.write("                    <ol class=\"carousel-indicators indicadores\">\r\n");
      out.write("                        <li data-target=\"#carousel-1\" data-slide-to=\"\"></li>\r\n");
      out.write("\r\n");
      out.write("                    </ol>\r\n");
      out.write("                </div>\r\n");
      out.write("            </div>\r\n");
      out.write("            <br>\r\n");
      out.write("            <br>\r\n");
      out.write("        ");
      org.apache.jasper.runtime.JspRuntimeLibrary.include(request, response, "Footer.jsp", out, false);
      out.write("\r\n");
      out.write("        <!-- //footer -->\r\n");
      out.write("        <a href=\"#home\" class=\"scroll\" id=\"toTop\" style=\"display: block;\"> <span id=\"toTopHover\" style=\"opacity: 1;\"> </span></a>\r\n");
      out.write("        <!-- js -->\r\n");
      out.write("        <script type=\"text/javascript\" src=\"js/jquery-2.1.4.min.js\"></script>\r\n");
      out.write("        <!-- //js -->\r\n");
      out.write("        <script src=\"js/modernizr.custom.js\"></script>\r\n");
      out.write("        <!-- Custom-JavaScript-File-Links --> \r\n");
      out.write("        <!-- script for responsive tabs -->\t\t\t\t\t\t\r\n");
      out.write("        <script src=\"js/easy-responsive-tabs.js\"></script>\r\n");
      out.write("        <script>\r\n");
      out.write("            $(document).ready(function() {\r\n");
      out.write("                $('#horizontalTab').easyResponsiveTabs({\r\n");
      out.write("                    type: 'default', //Types: default, vertical, accordion           \r\n");
      out.write("                    width: 'auto', //auto or any width like 600px\r\n");
      out.write("                    fit: true, // 100% fit in a container\r\n");
      out.write("                    closed: 'accordion', // Start closed if in accordion view\r\n");
      out.write("                    activate: function(event) { // Callback function if tab is switched\r\n");
      out.write("                        var $tab = $(this);\r\n");
      out.write("                        var $info = $('#tabInfo');\r\n");
      out.write("                        var $name = $('span', $info);\r\n");
      out.write("                        $name.text($tab.text());\r\n");
      out.write("                        $info.show();\r\n");
      out.write("                    }\r\n");
      out.write("                });\r\n");
      out.write("                $('#verticalTab').easyResponsiveTabs({\r\n");
      out.write("                    type: 'vertical',\r\n");
      out.write("                    width: 'auto',\r\n");
      out.write("                    fit: true\r\n");
      out.write("                });\r\n");
      out.write("            });\r\n");
      out.write("        </script>\r\n");
      out.write("        <!-- //script for responsive tabs -->\t\t\r\n");
      out.write("        <!-- stats -->\r\n");
      out.write("        <script src=\"js/jquery.waypoints.min.js\"></script>\r\n");
      out.write("        <script src=\"js/jquery.countup.js\"></script>\r\n");
      out.write("        <script>\r\n");
      out.write("            $('.counter').countUp();\r\n");
      out.write("        </script>\r\n");
      out.write("        <!-- //stats -->\r\n");
      out.write("        <!-- start-smoth-scrolling -->\r\n");
      out.write("        <script type=\"text/javascript\" src=\"js/move-top.js\"></script>\r\n");
      out.write("        <script type=\"text/javascript\" src=\"js/jquery.easing.min.js\"></script>\r\n");
      out.write("        <script type=\"text/javascript\">\r\n");
      out.write("            jQuery(document).ready(function($) {\r\n");
      out.write("                $(\".scroll\").click(function(event) {\r\n");
      out.write("                    event.preventDefault();\r\n");
      out.write("                    $('html,body').animate({scrollTop: $(this.hash).offset().top}, 1000);\r\n");
      out.write("                });\r\n");
      out.write("            });\r\n");
      out.write("        </script>\r\n");
      out.write("        <!-- here stars scrolling icon -->\r\n");
      out.write("        <script type=\"text/javascript\">\r\n");
      out.write("            $(document).ready(function() {\r\n");
      out.write("                /*\r\n");
      out.write("                 var defaults = {\r\n");
      out.write("                 containerID: 'toTop', // fading element id\r\n");
      out.write("                 containerHoverID: 'toTopHover', // fading element hover id\r\n");
      out.write("                 scrollSpeed: 1200,\r\n");
      out.write("                 easingType: 'linear' \r\n");
      out.write("                 };\r\n");
      out.write("                 */\r\n");
      out.write("\r\n");
      out.write("                $().UItoTop({easingType: 'easeOutQuart'});\r\n");
      out.write("\r\n");
      out.write("            });\r\n");
      out.write("        </script>\r\n");
      out.write("        <!-- //here ends scrolling icon -->\r\n");
      out.write("        <!-- for bootstrap working -->\r\n");
      out.write("        <script type=\"text/javascript\" src=\"js/bootstrap.js\"></script>\r\n");
      out.write("    </body>\r\n");
      out.write("\r\n");
      out.write("</html>\r\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
