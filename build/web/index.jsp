<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Readers Colections</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="keywords" content="Elite Shoppy Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
              Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
        <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
            function hideURLbar(){ window.scrollTo(0,1); } </script>
        <link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/estilos.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/font-awesome.css" rel="stylesheet"> 
        <link href="css/easy-responsive-tabs.css" rel='stylesheet' type='text/css'/>
       
    </head>
    <body>
        <jsp:include  page="Header.jsp"></jsp:include>
        <jsp:include  page="Menu.jsp"></jsp:include>
            <!-- banner -->
            <br>
            <br>
            <div class="container">
                <div id="carousel-1" class="carousel slide" data-ride="carousel">
                    <div class="carousel-inner" role="listbox">
                        <div class="item active"  align="center">
                            <img src="Imagenes/Slider/portada5.jpg" class="img-responsive" alt=""  >
                            <div class="container">
                                <div class="carousel-caption"><p class="contenido-slider">Portada 1</p></div>
                            </div>
                        </div>
                        <div class="item"  align="center">
                            <img src="Imagenes/Slider/portada1.jpg" class="img-responsive" alt=""  >
                            <div class="container">
                                <div class="carousel-caption"><p class="contenido-slider">Portada 1</p></div>
                            </div>
                        </div>
                        <div class="item"  align="center">
                            <img src="Imagenes/Slider/portada2.jpg" class="img-responsive" alt=""  >
                            <div class="container">
                                <div class="carousel-caption"><p class="contenido-slider">Portada 1</p></div>
                            </div>
                        </div>
                        <div class="item"  align="center">
                            <img src="Imagenes/Slider/portada3.jpg" class="img-responsive" alt=""  >
                            <div class="container">
                                <div class="carousel-caption"><p class="contenido-slider">Portada 1</p></div>
                            </div>
                        </div>
                        <div class="item"  align="center">
                            <img src="Imagenes/Slider/portada4.jpg" class="img-responsive" alt=""  >
                            <div class="container">
                                <div class="carousel-caption"><p class="contenido-slider">Portada 1</p></div>
                            </div>
                        </div>
                    </div>
                    <!-- Controls -->
                    <a href="#carousel-1" class="left carousel-control" role="button" data-slide="prev">
                        <span class="icon-prev"></span>
                    </a>
                    <a href="#carousel-1" class="right carousel-control" data-slide="next">
                        <span class="icon-next"></span>
                    </a>
                    <!-- Indicators -->
                    <ol class="carousel-indicators indicadores">
                        <li data-target="#carousel-1" data-slide-to=""></li>

                    </ol>
                </div>
            </div>
            <br>
            <br>
        <jsp:include  page="Footer.jsp"></jsp:include>
        <!-- //footer -->


        <a href="#home" class="scroll" id="toTop" style="display: block;"> <span id="toTopHover" style="opacity: 1;"> </span></a>
        <!-- js -->
        <script type="text/javascript" src="js/jquery-2.1.4.min.js"></script>
        <!-- //js -->
        <script src="js/modernizr.custom.js"></script>
        <!-- Custom-JavaScript-File-Links --> 
        <!-- script for responsive tabs -->						
        <script src="js/easy-responsive-tabs.js"></script>
        <script>
            $(document).ready(function() {
                $('#horizontalTab').easyResponsiveTabs({
                    type: 'default', //Types: default, vertical, accordion           
                    width: 'auto', //auto or any width like 600px
                    fit: true, // 100% fit in a container
                    closed: 'accordion', // Start closed if in accordion view
                    activate: function(event) { // Callback function if tab is switched
                        var $tab = $(this);
                        var $info = $('#tabInfo');
                        var $name = $('span', $info);
                        $name.text($tab.text());
                        $info.show();
                    }
                });
                $('#verticalTab').easyResponsiveTabs({
                    type: 'vertical',
                    width: 'auto',
                    fit: true
                });
            });
        </script>
        <!-- //script for responsive tabs -->		
        <!-- stats -->
        <script src="js/jquery.waypoints.min.js"></script>
        <script src="js/jquery.countup.js"></script>
        <script>
            $('.counter').countUp();
        </script>
        <!-- //stats -->
        <!-- start-smoth-scrolling -->
        <script type="text/javascript" src="js/move-top.js"></script>
        <script type="text/javascript" src="js/jquery.easing.min.js"></script>
        <script type="text/javascript">
            jQuery(document).ready(function($) {
                $(".scroll").click(function(event) {
                    event.preventDefault();
                    $('html,body').animate({scrollTop: $(this.hash).offset().top}, 1000);
                });
            });
        </script>
        <!-- here stars scrolling icon -->
        <script type="text/javascript">
            $(document).ready(function() {
                /*
                 var defaults = {
                 containerID: 'toTop', // fading element id
                 containerHoverID: 'toTopHover', // fading element hover id
                 scrollSpeed: 1200,
                 easingType: 'linear' 
                 };
                 */

                $().UItoTop({easingType: 'easeOutQuart'});

            });
        </script>
        <script type="text/javascript" src="js/bootstrap.js"></script>
    </body>
</html>
